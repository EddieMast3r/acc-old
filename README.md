# NowAccomplish
With the ACCOMPLISH app, find the professional advice your start-up needs from your neighborhood's brightest minds. 
And as one of them, share your expertise to get paid for your time! 

## Getting started
```
$ yarn install

/ios
$ pod install
```

## launch iOS
``$ react-native run-ios``

## launch Android
``$ react-native run-android``

## Libreries used

######  Flux architecture
  -  [Redux](https://redux.js.org/introduction)

######  Routing and navigation**
  -  [React Native Navigtation from Wix](https://github.com/wix/react-native-navigation)   

######  Firebase
  -  [React Native Firebase](https://github.com/invertase/react-native-firebase)  

## Style guidelines
  - https://eslint.org/
  - https://github.com/airbnb/javascript
  - https://github.com/airbnb/javascript/tree/master/react
  

:v: **Enjoy!**
