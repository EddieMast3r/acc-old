// skills
export const GET_SKILLS = 'GET_SKILLS';
export const SET_USER = 'SET_USER';
export const SET_USER_TOKEN = 'SET_USER_TOKEN';
export const GET_MESSAGE = 'GET_MESSAGE';
export const MESSAGE_LIST = 'MESSAGE_LIST';
// Notifications
export const GET_FROM_NOTIFICATION = 'GET_FROM_NOTIFICATION';
export const GET_TO_NOTIFICATION = 'GET_TO_NOTIFICATION';
// People
export const GET_PEOPLE = 'GET_PEOPLE';
export const GET_PEOPLE_NERBY = 'GET_PEOPLE_NEARBY';
