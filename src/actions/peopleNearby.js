import GeoLib from 'geolib';
import PeopleNearbyServices from '../provider/people/PeopleNearbyServices';
import { GET_PEOPLE_NERBY } from './types';

export const actGetPeopleNerbay = ( miles = 30 ) => ( dispatch ) => {
	PeopleNearbyServices.getPeople( ( people ) => {
		navigator.geolocation.getCurrentPosition( ( position ) => {
			const { coords: { latitude, longitude } } = position;
			const keys = Object.keys( people );
			const peopleInfo = keys.map( k => ( { id: k, ...people[ k ] } ) )
				.filter( ( f ) => {
					if ( typeof f.location !== 'undefined' ) {
						const { location: { coords } } = f;
						const distance = GeoLib.getDistance( {
							latitude,
							longitude
						},
						{ latitude: coords.latitude, longitude: coords.longitude } );
						const distanceMile = GeoLib.convertUnit( 'mi', distance );
						if ( distanceMile <= miles ) {
							return f;
						}
					}
					return false;
				} );

			dispatch( {
				type: GET_PEOPLE_NERBY,
				payload: peopleInfo
			} );
		} );
	} );
};
